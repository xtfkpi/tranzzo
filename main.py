w = 12


def selfsorted(arr: list) -> list:
    flag = True
    while flag:
        flag = False
        for i in range(0, len(arr) - 1):
            if (arr[i] > arr[i + 1]):
                arr[i], arr[i + 1] = arr[i + 1], arr[i]
                flag = True
    return arr


def get_edge(arr: list, w:int=12) -> int:
    summ = 0
    assert (len(arr) > 0)
    for i in range(0, len(arr)):
        if (summ + arr[i] > w):
            return i - 1
        summ = summ + arr[i]
    return i


def walk_node(tree, bonus=[]):
    global w
    leaves = selfsorted(tree["leaves"] + bonus)
    edge = get_edge(tree["leaves"])
    tree["leaves"] = leaves[0:edge + 1]
    bonus = leaves[edge + 1:]
    for subs in range(0, len(tree["nodes"])):
        bonus = walk_node(tree["nodes"][subs], bonus)
    return bonus


if __name__ == "__main__":
    tree = {
        "leaves": [3, 2, 5, 1], "nodes":
            [
                {"leaves": [3], "nodes": [
                    {"leaves": [10, 3, 8], "nodes": []},
                    {"leaves": [2, 8, 1, 4], "nodes": []}
                ]}
            ]
    }

    arr = [2, 6, 0, 2, 0, 9, 1]
    print(selfsorted(arr) == sorted(arr))
    walk_node(tree)
    print(tree)

def test_p00():
    assert True


def test_selfsort01():
    arr = [1, 7, 2, 4, 9]
    assert sorted(arr) == selfsorted(arr)


def test_selfsort02():
    arr = [0, 1, 0, 1, 0, 1]
    assert sorted(arr) == selfsorted(arr)

def test_edge():
    assert get_edge([1,2,3,8,12],6)==2

def test_tree():
    global w
    w=6
    tree = {"leaves":[1,2,3,8,12],"nodes":[]}
    bonus = walk_node(tree)
    assert tree["leaves"]==[1,2,3]
    assert bonus == [8,12]


